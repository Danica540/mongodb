const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const logger = require('morgan');
const expressHandlebars = require('express-handlebars');
const express = require('express');
const Handlebars = require('handlebars');

const app = express();
const port = process.env.PORT || 3000;

// Seting up morgan
app.use(logger('dev'));

// Seting up bodyParser
app.use(bodyParser.urlencoded({
    extended: false
}));

// Seting up statis files for usage
app.use(express.static('public'));

// Seting view engine
app.engine("handlebars", expressHandlebars({
    defaultLayout: "main"
}));
app.set("view engine", "handlebars");

// Custom helpers
Handlebars.registerHelper('ifValue', function (val1, val2, options) {
    if (val1 == val2) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
Handlebars.registerHelper('ifNotValue', function (val1, val2, options) {
    if (val1 != val2) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

// Connecting to mongoose
mongoose.connect("mongodb://localhost/scraped");
const db = mongoose.connection;

db.on('error',()=>console.log("An error occured"));
db.once('open',()=>{
    console.log("Connected to mongoose")
})


var routes = require("./controller/controller.js");
app.use("/", routes);

// Listening on port
app.listen(port, () => {
    console.log("Server listening on port " + port);
})

