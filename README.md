# mongoDB
- Pokrenuti komandu ```npm init```
- Pokrenuti aplikaciju komandom ```node server.js```
- Pre otvaranja pocetne stranice aplikacije na ruti ```localhost:3000``` otvoriti rutu ```localhost:3000/scrape``` da bi se baza napunila podacima sa sajtova.
  Obzirom da ovaj proces traje nekoliko minuta(u zavisnosti od internet konekcije moze da traje i do 15 i vise minuta), proces slobodno prekinuti nakon minut-dva i otvoriti rutu ```localhost:3000```
- Nelogovani korisnik moze da pretrazuje laptopove i vidi koje su cene u raznim radnjama
  (Npr. Uneti laptop ```Acer A315-42-R52R```), a moze i da gleda sav asortiman radnja odabirom radnje u meniju.
- Logovani korisnik pored pretrazivanja ima opciju i da sacuva laptopove koji mu se svidjaju u listu.
- Korisnik ima opciju i da obrise svoj nalog. 

