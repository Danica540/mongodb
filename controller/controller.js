const express = require('express');
const path = require('path');
const router = express.Router();
const cheerio = require('cheerio');
const request = require('request');
const Article = require('../models/Article');
const User = require('../models/User');

let isUserSignedIn = false;
let signedUser = {};


router.get('/', (req, res) => {
  let count = 0;
  if (signedUser.favorites) {
    count = signedUser.favorites.length;
  }
  res.render("home", { isUserSignedIn: isUserSignedIn, userFavorites: signedUser.favorites, count: count });
})

router.get("/scrape", (req, res) => {
  let pageCounter = 1;
  let promiseArray = [];
  while (pageCounter <= 4) {
    promiseArray.push(parseWinWinPage(pageCounter));
    pageCounter++;
  }
  pageCounter = 0;
  let limit = 0;
  while (pageCounter <= 10) {
    promiseArray.push(parseJakovSistemPage(limit));
    pageCounter += 1;
    limit += 15;
  }
  promiseArray.push(parseEmmiPage());
  Promise.all(promiseArray).then(() => {
    console.log("Zavrseno scrape-ovanje.")
    res.redirect("/");
  })

});

router.get('/register', (req, res) => {
  res.render("register");
})

router.get('/unregister', (req, res) => {
  isUserSignedIn = false;
  signedUser = {};
  count = 0;
  res.render("home", { isUserSignedIn: isUserSignedIn, userFavorites: signedUser.favorites, count: count });
})

router.get('/deleteUser', (req, res) => {
  User.remove({ username: signedUser.username }).exec((err, doc) => {
    if (err) {
      console.log(err);
    }
  })
  isUserSignedIn = false;
  signedUser = {};
  count = 0;
  res.render("home", { isUserSignedIn: isUserSignedIn, userFavorites: signedUser.favorites, count: count });
})

router.post('/signUp', (req, res) => {
  console.log(req.body.usernameSignUp);
  console.log(req.body.passwordSignUp);
  const username = req.body.usernameSignUp;
  const password = req.body.passwordSignUp;
  User.findOne({ username: username })
    .exec((err, doc) => {
      if (err) {
        console.log(err);
      }
      else {
        if (!doc) {
          // ne postoji korisnik sa tim imenom
          let userTmp = {};
          userTmp.username = username;
          userTmp.password = password;
          user.favorites = [];
          let user = new User(userTmp);
          user.save(function (err, doc) {
            if (err) {
              console.log(err);
            }
          });
          isUserSignedIn = true;
          signedUser = userTmp;
          let count = 0;
          if (signedUser.favorites) {
            count = signedUser.favorites.length;
          }
          res.render("home", { isUserSignedIn: isUserSignedIn, userFavorites: signedUser.favorites, count: count });
        }
        else {
          res.render("register", { error: "Zauzeto korisnicko ime." });
        }
      }
    })
})

router.post('/signIn', (req, res) => {
  console.log(req.body.usernameSignIn);
  console.log(req.body.passwordSignIn);
  const username = req.body.usernameSignIn;
  const password = req.body.passwordSignIn;
  User.findOne({ username: username, password: password })
    .populate("favorites")
    .exec((err, doc) => {
      if (err) {
        console.log(err);
      }
      else {
        if (!doc) {
          // ne postoji korisnik sa tim imenom i lozinkom
          res.render("register", { error: "Nevalidno korisnicko ime ili lozinka." });
        }
        else {
          isUserSignedIn = true;
          signedUser = doc;
          let count = 0;
          if (signedUser.favorites) {
            count = signedUser.favorites.length;
          }
          res.render("home", { isUserSignedIn: isUserSignedIn, userFavorites: signedUser.favorites, count: count });
        }
      }
    })
})

router.get("/emmi", (req, res) => {
  renderStorePage(res, "Emmi");
});

router.get("/winwin", (req, res) => {
  renderStorePage(res, 'Winwin');
});

router.get("/jakov_sistem", (req, res) => {
  renderStorePage(res, 'Jakov sistem');
});

router.post("/removeFromFavorites", (req, res) => {
  const username = req.body.username;
  const title = req.body.title;
  const store = req.body.store;

  Article.findOne({ title: title, store: store })
    .exec((err, doc) => {
      if (err) {
        console.log(err);
      }
      else {
        User.findOneAndUpdate(
          { username: username },
          { $pull: { favorites: doc._id } },
          { new: true }
        ).populate("favorites").exec((err, document) => {
          if (err) {
            console.log(err);
          } else {
            signedUser.favorites = document.favorites
            res.render("details", { article: doc, isUserSignedIn: isUserSignedIn, isAddedToFavorites: false, user: signedUser });
          }
        });
      }
    })
});

router.post("/addToFavorites", (req, res) => {
  const username = req.body.username;
  const title = req.body.title;
  const store = req.body.store;

  Article.findOne({ title: title, store: store })
    .exec((err, doc) => {
      if (err) {
        console.log(err);
      }
      else {
        User.findOneAndUpdate(
          { username: username },
          { $push: { favorites: doc._id } },
          { new: true }
        ).exec((err, document) => {
          if (err) {
            console.log(err);
          } else {
            signedUser.favorites.push(doc);
            res.render("details", { article: doc, isUserSignedIn: isUserSignedIn, isAddedToFavorites: true, user: signedUser });
          }
        });
      }
    })
});

router.post("/find", (req, res) => {
  const searchString = req.body.searchstr;
  let query = { title: { $regex: ".*" + searchString + ".*" } };
  Article.find(query)
    .sort({ price: 1 })
    .exec((err, doc) => {
      if (err) {
        console.log(err);
      } else {
        let articles = JSON.parse(JSON.stringify(doc));
        let count = 0;
        if (signedUser.favorites) {
          count = signedUser.favorites.length;
        }
        res.render("home", { articles: articles, isUserSignedIn: isUserSignedIn, userFavorites: signedUser.favorites, count: count });
      }
    });

});

router.post("/details", (req, res) => {
  const articleTitle = req.body.title;
  const articleStore = req.body.store;
  let query = { title: articleTitle, store: articleStore };
  Article.findOne(query)
    .exec(function (err, doc) {
      if (err) {
        console.log(err);
      } else {
        let article = JSON.parse(JSON.stringify(doc));
        let result = article;
        let details = {};
        if (article && article.link) {
          let link = article.link;
          if (articleStore == "Emmi") {
            link = "https://www.emmi.rs/laptop-ra%C4%8Dunari-asus-zenbook-flip-um462da-ai012t-win10-14touch.11.html?productId=87116";
          }
          request(link, (error, response, html) => {
            if (html) {
              const $ = cheerio.load(html);
              if (articleStore == "Winwin") {
                $("tr").each(function (i, element) {
                  let propertyName = $("th.label", this)
                    .text();
                  propertyName = formatPropertyName(propertyName);
                  details[propertyName] = $("td.data", this)
                    .text();
                });
              }
              else if (articleStore == "Jakov sistem") {
                $(".neparno").each(function (i, element) {
                  let propertyName = $("div.atribut", this)
                    .text();
                  propertyName = formatPropertyName(propertyName);
                  details[propertyName] = $("div.vrednost", this)
                    .text();
                });
                $(".parno").each(function (i, element) {
                  let propertyName = $("div.atribut", this)
                    .text();
                  propertyName = formatPropertyName(propertyName);
                  details[propertyName] = $("div.vrednost", this)
                    .text();
                })
              }
              else {
                // Emmi
                $("tr").each(function (i, element) {
                  let nizAtributVrednost = [];
                  $("td", this).each(function (i, element) {
                    let property = $(this).text().replace(/(\r\n|\n|\r|\t\t|\t)/gm, "");;
                    if (i == 0) {
                      property = formatPropertyName(property);
                      nizAtributVrednost.push(property);
                    }
                    else {
                      nizAtributVrednost.push(property);
                    }
                  });
                  details[nizAtributVrednost[0]] = nizAtributVrednost[1];
                });
              }
              result.details = details;
              let isAddedToFavorites = false;
              if (signedUser.favorites) {
                signedUser.favorites.forEach(articl => {
                  if (articl._id == result._id) {
                    isAddedToFavorites = true;
                  }
                })
              }
              Article.updateOne({ title: result.title, store: result.store }, result).exec((err, doc) => {
                if (err) {
                  console.log(err);
                }
              });
              res.render("details", { article: result, isUserSignedIn: isUserSignedIn, isAddedToFavorites: isAddedToFavorites, user: signedUser });
            }
            else {
              res.render("error", { error: "Ne postoje detalji za ovaj proizvod. Proverite internet konekciju." })
              return;
            }

          });
        }
        else {
          res.render("error", { error: "Ne postoje detalji za ovaj proizvod" })
          return;
        }
      }
    });

});


function renderStorePage(res, storeName) {
  let query = { store: storeName };
  Article.find(query)
    .sort({ price: 1 })
    .exec((err, doc) => {
      if (err) {
        console.log(err);
      } else {
        let articles = JSON.parse(JSON.stringify(doc));
        articles.pop();
        articles.pop();
        if (storeName == "Winwin")
          res.render("articles", { store: "Winwin", articles: articles, count: articles.length });
        else if (storeName == "Emmi")
          res.render("articles", { store: "Emmi", articles: articles, count: articles.length });
        else if (storeName == "Jakov sistem")
          res.render("articles", { store: "Jakov sistem", articles: articles, count: articles.length });
        else
          res.render("error", { error: "Ne postoji prodavnica sa tim imenom." });
      }
    });
}

function parseWinWinPage(pageIndex) {
  return new Promise(() => {
    request("https://www.winwin.rs/laptop-i-tablet-racunari/laptop-notebook-racunari.html?limit=30&p=" + pageIndex, (error, response, html) => {
      var $ = cheerio.load(html);
      var titlesArray = [];

      $("li.item").each(function (i, element) {
        var result = {};

        let title = $("h2.product-name", this)
          .children("a")
          .text().replace("Laptop", "");
        result.title = title.substr(0, title.indexOf(','));
        result.link = $("h2.product-name", this)
          .children("a")
          .attr("href");
        result.price = $("span.price", this)
          .text();
        result.store = "Winwin";
        checkResultAndSave(result, titlesArray);

      });
    });
  });
}

function parseEmmiPage() {
  return new Promise(() => {
    request("https://www.emmi.rs/konfigurator/proizvodi.10.html?go=true&Id=10&categoryId=127&limit=-1&offset=0", (error, response, html) => {
      var $ = cheerio.load(html);
      var titlesArray = [];

      $(".productListItem").each(function (i, element) {
        var result = {};

        result.title = $("div.productListTitle", this)
          .children("a")
          .text();
        result.link = "https://www.emmi.rs" + $("div.productListTitle", this)
          .children("a")
          .attr("href");
        result.price = $("span.price", this)
          .text();
        result.store = "Emmi";
        checkResultAndSave(result, titlesArray);
      });
    });
  });
}

function parseJakovSistemPage(pageIndex) {
  return new Promise(() => {
    const link = "https://www.jakovsistem.com/laptop-racunari/laptopovi?limit=15&limitstart=" + pageIndex;
    request(link, (error, response, html) => {
      var $ = cheerio.load(html);
      var titlesArray = [];

      $(".browseProductContainer").each(function (i, element) {
        var result = {};

        result.title = $("div.browseProductTitle", this)
          .children("a")
          .text();
        result.link = "https://www.jakovsistem.com" + $("div.browseProductTitle", this)
          .children("a")
          .attr("href");
        result.price = $("span.productPrice", this)
          .text().replace(/(\r\n|\n|\r|\t\t|\t)/gm, "");
        result.store = "Jakov sistem";
        checkResultAndSave(result, titlesArray);
      });
    });
  });
}

function checkResultAndSave(result, titlesArray) {
  // Proverava duplikate ili prazne delove sa cenom
  if (result.title !== "" && result.price !== "") {
    if (titlesArray.indexOf(result.title) == -1) {
      titlesArray.push(result.title);

      Article.countDocuments({ title: result.title, store: result.store }, (err, test) => {
        // Ako se ne nalazi u bazi, dodaj ga
        if (test === 0) {
          var entry = new Article(result);

          entry.save(function (err, doc) {
            if (err) {
              console.log(err);
            }
          });
        }
        else {
          // Ako se nalazi u bazi update-uj ga
          Article.updateOne({ title: result.title, store: result.store }, {
            $set: {
              price: result.price,
              link: result.link
            }
          }).exec((err, doc) => {
            if (err) {
              console.log(err)
            }
          });
        }
      });
    } else {
      console.log("Article already exists.");
    }
  } else {
    console.log("Not saved to DB, missing data");
  }
}

function formatPropertyName(propertyName) {
  propertyName = propertyName
    .replace("ć", "c")
    .replace("č", "c")
    .replace("ž", "z")
    .replace("đ", "dj")
    .replace("š", "s");
  propertyName = propertyName.toLowerCase();
  propertyName = titleCase(propertyName).replace(/\s/g, "");
  propertyName = propertyName.charAt(0).toLowerCase() + propertyName.slice(1);
  return propertyName;
}

function titleCase(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}


module.exports = router;