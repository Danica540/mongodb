const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const ArticleSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    store: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    link: {
        type: String,
        required: false
    },
    details: {
        type: Object,
        required: false
    }

})
const Article = mongoose.model('Article', ArticleSchema);
module.exports = Article;
